# README #

pymobiledevice is a cross-platform implementation of the mobiledevice library 
that talks the protocols to support iPhone®, iPod Touch®, iPad® and Apple TV® devices.


pymobiledevice
==============

This repository is a fork of [iOSForensics/pymobiledevice](https://github.com/iOSForensics/pymobiledevice) for Python 3 without M2Crypto dependency.

Requirements
--------------

 * Python 3
 * pyOpenSSL
 * Construct
 * pyasn1


Lockdownd.py [com.apple.lockownd]
--------------
This script can be used in order to pair with the device & starts other services.
    
*/!\ Others services can only being accessed after succesful pairing.
Succesful pairing requiert the device to be unlocked and user to click on 
"Trust this device" on its phone screen.*
     

afc.py [com.apple.afc]
--------------
This service is responsible for things such as copying music and photos. AFC Clients like iTunes 
are allowed accessing to a “jailed” or limited area of the device filesystem. Actually, AFC clients can 
only access certain files, namely those located in the Media folder.
